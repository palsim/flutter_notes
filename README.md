# flutter_notes

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## DDD: Domain-Driven Design

$ flutter pub run build_runner watch --delete-conflicting-outputs

- freezed
- dartz
- functional programming
- facade
- union: Either
- Unit: "is like" void (empty tuple)

$ keytool -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore

- add google-services.json to .gitignore

- proj gradle
  add
  classpath 'com.google.gms:google-services:4.3.10'

- module gradle
  add
  apply plugin: 'com.google.gms.google-services'

and add
implementation 'com.google.firebase:firebase-analytics'

### firebase

$ cd /opt/android-studio/jre/bin
$ ./keytool -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore

- then download json file from firebase!

-[sound null safety](https://fluttercorner.com/cannot-run-with-sound-null-safety-because-dependencies-dont-support-null-safety)

//EmailAddress and Password are value objects - self validation
//User is a entity - can have multiple value objects (self validate)

[auto_route error after flutter upgrade](https://github.com/Milad-Akarie/auto_route_library/issues/659)
$ flutter pub upgrade
