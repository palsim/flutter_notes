import 'package:dartz/dartz.dart';
import '../core/failures.dart';
import '../core/value_objects.dart';
import '../core/value_validators.dart';

class EmailAddress extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory EmailAddress(String input) {
    return EmailAddress._(
      validateEmailAddress(input),
    );
  }

  const EmailAddress._(this.value);
}

class Password extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory Password(String input) {
    return Password._(
      validatePassword(input),
    );
  }

  const Password._(this.value);
}


// UI use cases
// void showingTheEmailAddressOrFailure() {
//   final emailAddress = EmailAddress("invalidEmail@");

//   String emailText = emailAddress.value.fold(
//     (left) => "Failure happened, nore precisely: $left",
//     (right) => right,
//   );

//   String emailText2 = emailAddress.value.getOrElse(
//     () => "Failure happened",
//   );

//   // ignore: avoid_print
//   print(emailText);
//   // ignore: avoid_print
//   print(emailText2);
// }
