import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import '../../../../application/notes/note_watcher/note_watcher_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UncompletedSwitch extends HookWidget {
  const UncompletedSwitch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final uncompletedSwitchState = useState(
    //   IconSwitchData(
    //       toggle: false, icon: const Icon(Icons.indeterminate_check_box)),
    // );

    final toogleState = useState(false);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: InkResponse(
        onTap: () {
          toogleState.value = !toogleState.value;

          // final s = uncompletedSwitchState;
          // s.value = s.value.copyWith(toggle: !s.value.toggle);
          // s.value = s.value.toggle
          //     ? s.value.copyWith(
          //         icon: const Icon(
          //         Icons.check_box_outline_blank,
          //         key: Key('outline'),
          //       ))
          //     : s.value.copyWith(
          //         icon: const Icon(
          //         Icons.indeterminate_check_box,
          //         key: Key('indeterminate'),
          //       ));
          // performAction(context, uncompleted: s.value.toggle);
          performAction(context, uncompleted: toogleState.value);
        },
        child: AnimatedSwitcher(
          duration: const Duration(milliseconds: 200),
          transitionBuilder: (child, animation) => ScaleTransition(
            scale: animation,
            child: child,
          ),
          //child: uncompletedSwitchState.value.icon,
          child: toogleState.value
              ? const Icon(
                  Icons.check_box_outline_blank,
                  key: Key('outline'),
                )
              : const Icon(
                  Icons.indeterminate_check_box,
                  key: Key('indeterminate'),
                ),
        ),
      ),
    );
  }

  void performAction(BuildContext context, {required bool uncompleted}) {
    context.read<NoteWatcherBloc>().add(
          uncompleted
              ? const NoteWatcherEvent.watchUncompletedStarted()
              : const NoteWatcherEvent.watchAllStarted(),
        );
  }
}
