import 'package:firebase_auth/firebase_auth.dart';

import '../../domain/auth/user.dart' as du;
import '../../domain/core/value_objects.dart';

extension FirebaseUserDomainX on User {
  du.User toDomain() {
    return du.User(id: UniqueId.fromUniqueString(uid));
  }
}
